﻿using System;
using System.Collections.Generic;
using System.Text;
using timesheet.model;

namespace timesheet.business
{
    public interface IEmployeeService
    {
        IEnumerable<Employee> GetEmployees();

        Employee GetEmployeeById(int employeeId);

        IEnumerable<EmployeeTimeLogModel> GetTimeSheetForGivenEmployee(int employeeId);

        void InsertTimeLogForGivenEmployee(int employeeId, IEnumerable<EmployeeTimeLogModel> timeLogs);
    }
}
