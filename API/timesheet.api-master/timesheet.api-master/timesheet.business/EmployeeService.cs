﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class EmployeeService : IEmployeeService
    {
        public TimesheetDb db { get; }
        public EmployeeService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public Employee GetEmployeeById(int employeeId)
        {
            return this.db.Employees.FirstOrDefault(emp => emp.Id == employeeId);
        }

        public IEnumerable<Employee> GetEmployees()
        {
            return this.db.Employees.ToList();
        }

        public IEnumerable<EmployeeTimeLogModel> GetTimeSheetForGivenEmployee(int employeeId)
        {
            List<EmployeeTimeLogModel> timeSheetForGivenEmployee = new List<EmployeeTimeLogModel>();
            (this.db.EmployeeTimeLog.Where(i => i.Employee == employeeId)).ToList().ForEach(item =>
            {
                timeSheetForGivenEmployee.Add(new EmployeeTimeLogModel()
                {
                    Weekday = this.db.Weekdays.FirstOrDefault(i => i.WeekId == item.Weekday),
                    Effort = item.Effort,
                    Task = this.db.Tasks.FirstOrDefault(i => i.Id == item.Task)
                });
            });

            return timeSheetForGivenEmployee;
        }

        public void InsertTimeLogForGivenEmployee(int employeeId, IEnumerable<EmployeeTimeLogModel> timeLogs)
        {
            List<EmployeeTimeLog> logs = new List<EmployeeTimeLog>();
            timeLogs.ToList().ForEach(item => {

                logs.Add(new EmployeeTimeLog()
                {
                    Employee = employeeId,
                    Effort = item.Effort,
                    Task = item.Task.Id,
                    Weekday = item.Weekday.WeekId
                });
            });

            //Update the timeLogs for given employees
            this.db.EmployeeTimeLog.UpdateRange(logs);

            this.db.SaveChanges();
        }
    }
}
