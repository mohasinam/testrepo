﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Route("api/v1/employee")]
    [ApiController]
    public class EmployeeController : Controller
    {
        public IEmployeeService employeeService;
        public EmployeeController(IEmployeeService employeeService)
        {
            this.employeeService = employeeService;
        }

        /// <summary>
        /// Get All Employee List
        /// </summary>
        /// <returns></returns>
        [HttpGet("getall")]
        public IActionResult GetAllEmployees()
        {
            var items = this.employeeService.GetEmployees();
            return new ObjectResult(items);
        }

        /// <summary>
        /// Get TimeSheet Entry for each Task in each week day for a specific employee
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/TimeLog")]
        public IActionResult GetTimeLogForGivenEmployee(int id)
        {
            if (id <= 0) return BadRequest(new { message = "Given EmployeeId is Invalid" });
            if (this.employeeService.GetEmployeeById(id) == null) return NotFound(new { message = "Employee with given Id not found" });
            var items = this.employeeService.GetTimeSheetForGivenEmployee(id);
            return new ObjectResult(items);
        }

        /// <summary>
        /// Insert Time entry for a employee
        /// </summary>
        /// <param name="id"></param>
        /// <param name="timeLogs"></param>
        /// <returns></returns>
        [HttpPost("{id}/TimeLog")]
        public IActionResult InsertTimeLogForGivenEmployee(int id, [FromBody]IEnumerable<EmployeeTimeLogModel> timeLogs)
        {
            if (id <= 0) return BadRequest(new { message = "Given EmployeeId is Invalid" });
            if (this.employeeService.GetEmployeeById(id) == null) return NotFound(new { message = "Employee with given Id not found" });
            this.employeeService.InsertTimeLogForGivenEmployee(id,timeLogs);
            return new ObjectResult("Inserted TimeLog Succesfully");
        }
    }
}