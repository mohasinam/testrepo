﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace timesheet.model
{
    [Table("WeekDays")]
    public class WeekDays
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Int16 WeekId { get; set; }

        [StringLength(10)]
        [Required]
        public string WeekName { get; set; }
    }
}
