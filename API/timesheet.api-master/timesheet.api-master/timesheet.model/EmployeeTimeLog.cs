﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace timesheet.model
{
    [Table("EmployeeTimeLog")]
    public class EmployeeTimeLog
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        [ForeignKey("Employee")]
        public int Employee { get; set; }
        [ForeignKey("WeekDays")]
        public Int16 Weekday { get; set; }
        [ForeignKey("Task")]
        public int Task { get; set; }
        public int Effort { get; set; }

    }
}
