﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.model
{
    public class EmployeeTimeLogModel
    {
        public WeekDays Weekday { get; set; }
        public Task Task { get; set; }
        public int Effort { get; set; }
    }
}
