import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';

@Component({
    selector: 'employee-list',
    templateUrl: 'timesheet.component.html',
    styleUrls:['./timesheet.component.scss']
})

export class TimeSheetListComponent implements OnInit {
    employees: any;
    employeeId : number;
    timelog : any
    constructor(private employeeService: EmployeeService, employeeId : number) { 
        this.employeeId = employeeId;
    }

    ngOnInit() {
        this.employeeService.getallemployees().subscribe(data => {
            this.employees = data;
        });

        this.employeeService.getTimelogForEmployees(this.employeeId).subscribe(data => {
            this.timelog = data;
        })
    }
}